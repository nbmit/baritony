<?php
	/**
		* The header for our theme
		*
		* This is the template that displays all of the <head> section and everything up until <div id="content">
		*
		* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
		*
		* @package offereight
	*/
	
?>
<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		
		<?php wp_head(); ?>
	</head>
	
	<body <?php body_class(); ?>>
		<div id="page" class="site">
			<!--<a class="skip-link screen-reader-text" href="#content"><?php //esc_html_e( 'Skip to content', 'offereight' ); ?></a>-->
			
			<header id="masthead" class="site-header">					
				<div class="fw-row sb-top sb-head-top">				
					<div class="fw-container site-branding">	
						<div class="fw-col-xs-12 fw-col-sm-4 sb-left">
							<?php
								wp_nav_menu( array(
									'container'      => 'div',
								 	'menu_class'     => 'menu',
								    'theme_location' => 'menu-left-top',
								) );
							?>
						</div>
						
						<div class="fw-col-xs-12 fw-col-sm-4 sb-center">
							<?php echo get_custom_logo( );?>
							
						</div>
						
						<div class="fw-col-xs-12 fw-col-sm-4 sb-right">
							<?php
								wp_nav_menu( array(
									'container'      => 'div',
								 	'menu_class'     => 'menu',
								    'theme_location' => 'menu-right-top',
								) );
							?>
							
						</div>
					</div>
				</div>			
				<div class="fw-row sb-top">				
					<div class="fw-container site-branding">	
						<nav id="site-navigation" class="main-navigation">
							<?php
								wp_nav_menu( array(
									'container'      => 'div',
								 	'menu_class'     => 'menu',
								    'theme_location' => 'menu-main',
								) );
							?>
						</nav><!-- #site-navigation -->
					</div>
				</div>
					
			</header><!-- #masthead -->
				<?php
					//the_custom_logo();
					if ( is_front_page() && is_home() ) : ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
						endif;
						
						$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
					<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
					<?php
					endif; ?>
			</div><!-- .site-branding -->
			
			<div id="content" class="site-content">
				<!--<strong><?php //echo fw_get_db_settings_option('header_text') ?></strong>
					
					<strong><?php //echo fw_get_db_settings_option('footer_text') ?></strong>
					
				<strong><?php //echo fw_get_db_settings_option('bg-color') ?></strong> -->
						