(function($){
    $(document).ready(function(){
    	$( ".swiper-container" ).each(function( index ) {
    		id = $(this).attr("id");
    		var swiper = new Swiper('#' + id, {
		      pagination: {
		        el: '.swiper-pagination',
		      },
		      navigation: {
		        nextEl: '.swiper-button-next',
		        prevEl: '.swiper-button-prev',
		      },
		    });
		});
	})
})(jQuery);
