<?php
/**
 * offereight functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package offereight
 */

if ( ! function_exists( 'offereight_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function offereight_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on offereight, use a find and replace
		 * to change 'offereight' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'offereight', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-main' => esc_html__( 'Главное меню', 'offereight' ),
		) );
		// 
		register_nav_menus( array(
			'menu-left-top' => esc_html__( 'Меню контактов', 'offereight' ),
		) );
		// 
		register_nav_menus( array(
			'menu-right-top' => esc_html__( 'Социальные иконки', 'offereight' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'offereight_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'offereight_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function offereight_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'offereight_content_width', 640 );
}
add_action( 'after_setup_theme', 'offereight_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function offereight_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'offereight' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'offereight' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'offereight_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function offereight_scripts() {
	wp_enqueue_style( 'offereight-style', get_stylesheet_uri() );

	wp_enqueue_style( 'offereight-theme-style', get_template_directory_uri() . '/css/theme.css' );
	
	wp_enqueue_style( 'offereight-theme-media', get_template_directory_uri() . '/css/media.css' );
	
	wp_enqueue_script( 'offereight-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'offereight-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_script( 'offereight-swiper', get_template_directory_uri() . '/js/swiper.min.js', array(), '20151215', true );
	wp_enqueue_style( 'offereight-theme-media-swiper', get_template_directory_uri() . '/css/swiper.min.css' );

	wp_enqueue_script( 'offereight-main-script', get_template_directory_uri() . '/js/main.js', array(), '20151215', true );
}
add_action( 'wp_enqueue_scripts', 'offereight_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load TGM Plugins
*/
require get_template_directory() . '/tgm/offereight-theme.php';

add_action( 'wp_enqueue_scripts', 'enqueue_load_fa' );
function enqueue_load_fa() {
  
    wp_enqueue_style( 'load-fa', get_template_directory_uri() . '/css/fontawesome/css/font-awesome.min.css' );
}

add_action( 'wp_enqueue_scripts', 'enqueue_load_swiper' );
function enqueue_load_swiper() {
  
    
}
